import requests
import matplotlib.pyplot as plt
from Usuarios import Usuarios

def moduloSAS1():
    """
        La funcion trata sobre la toma de datos del usuario y la respuesta de esta

        nombre_nuevo: es el nombre que se va a tomar del usuario
        medico_nuevo: es el medico  que va a tomar el usuario
        preguntas: es una lista que va a tener las respuestas de las preguntas
        contador: el contador determina si la persona es No infectado, En revision, Posible infectado o Infectado
        comprobador: comprueba si el numero de telefono tiene el prefijo de un telefono venezolano

        Retorna los datos de la persona, si es que esta infectada y el extra(numero de telefono o lugar donde vive o medico)
    """

    nombre_nuevo =""
    medico_nuevo =""
    preguntas =[]
    contador =0
    comprobador = True

    #nombre va a valer lo que ponga el usuario
    nombre = input("Escriba su nombre: ")

    #se crea la variable nombre_espacios que va a ser una lista que contenga en elementos cada palabra de nombre
    nombre_espacios = nombre.split()

    #Bucle de los elementos de nombre_espacios
    for x in nombre_espacios:

        #nombre_nuevo va a ser todas las palabras de nombre juntas
        nombre_nuevo += x.lower().capitalize()

    #Mientras que el nombre_nuevo no sea de caracter alfabetico
    while not nombre_nuevo.isalpha():

        
        #nombre va a valer lo que ponga el usuario
        nombre = input("Escriba su nombre: ")

        #se crea la variable nombre_espacios que va a ser una lista que contenga en elementos cada palabra de nombre
        nombre_espacios = nombre.split()

        #Bucle de los elementos de nombre_espacios
        for x in nombre_espacios:

            #nombre_nuevo va a ser todas las palabras de nombre juntas
            nombre_nuevo += x.lower().capitalize()

    #se hace el llamado de la funcion verificar con el argumento del nombre_nuevo
    verificar(nombre_nuevo)

    #Edad va a ser el valor que el usuario introduzca
    edad = input("Escriba su edad: ")

    #Mientras que edad no sea un numero natural o que el numero sea mayor que 100 o menor que 5
    while not edad.isdecimal() or int(edad) <5 or int(edad) >100:

        #Edad va a ser el valor que el usuario introduzca
        edad = input("Escriba su edad: ")

    #La pregunta va a tener el valor que el usuario introduzca
    pregunta1 = input("¿Tiene secreciones nasales?Y/N").upper()

    #Mientras que el valor de la pregunta sea diferente de "Y" y "N"
    while pregunta1 != "Y" and pregunta1 !="N":

        #La pregunta va a tener el valor que el usuario introduzca
        pregunta1 = input("¿Tiene secreciones nasales?Y/N").upper()
    
    #El valor de la pregunta queda registrada en una lista
    preguntas.append(pregunta1)

    #La pregunta va a tener el valor que el usuario introduzca
    pregunta2 = input("¿Tiene dolor de garganta?Y/N").upper()

    #Mientras que el valor de la pregunta sea diferente de "Y" y "N"
    while pregunta2 != "Y" and pregunta2 !="N":

        #La pregunta va a tener el valor que el usuario introduzca
        pregunta2 = input("¿Tiene dolor de garganta?Y/N").upper()
    
    #El valor de la pregunta queda registrada en una lista
    preguntas.append(pregunta2)

    #La pregunta va a tener el valor que el usuario introduzca
    pregunta3 = input("¿Tiene tos?Y/N").upper()

    #Mientras que el valor de la pregunta sea diferente de "Y" y "N"
    while pregunta3 != "Y" and pregunta3 !="N":

        #La pregunta va a tener el valor que el usuario introduzca
        pregunta3 = input("¿Tiene tos?Y/N").upper()
    
    #El valor de la pregunta queda registrada en una lista
    preguntas.append(pregunta3)

    #La pregunta va a tener el valor que el usuario introduzca
    pregunta4 = input("¿Tienes fiebre?Y/N").upper()

    #Mientras que el valor de la pregunta sea diferente de "Y" y "N"
    while pregunta4 != "Y" and pregunta4 !="N":

        #La pregunta va a tener el valor que el usuario introduzca
        pregunta4 = input("¿Tienes fiebre?Y/N").upper()

    #El valor de la pregunta queda registrada en una lista
    preguntas.append(pregunta4)

    #La pregunta va a tener el valor que el usuario introduzca
    pregunta5 = input("¿Tienes dificultad para respirar?Y/N").upper()

    #Mientras que el valor de la pregunta sea diferente de "Y" y "N"
    while pregunta5 != "Y" and pregunta5 !="N":

        #La pregunta va a tener el valor que el usuario introduzca
        pregunta5 = input("¿Tienes dificultad para respirar?Y/N").upper()

    #El valor de la pregunta queda registrada en una lista
    preguntas.append(pregunta5)

    #Bucle de los elementos de preguntas
    for pregunta in preguntas:

        #Si la pregunta es similar a "Y"
        if pregunta == "Y":

            #Se le suma uno al contador
            contador += 1
    
    #Si el contador es similar a 5
    if contador == 5:

        #clasificacion sera igual a Infectado
        clasificacion = "Infectado"

    #Sino, si el contador es mayor a 2
    elif contador >2:

        #clasificacion sera igual a Posible infectado
        clasificacion = "Posible infectado"

    #Sino, si el contador es mayor o igual que 1
    elif contador >= 1:

        #clasificacion sera igual a En revision
        clasificacion = "En revision"

    #Sino, si el contador es similar a 0
    elif contador == 0:

        #clasificacion sera igual a No infectado
        clasificacion = "No infectado"
    
    #Si clasificacion es similar a No infectado o En revision
    if clasificacion == "No infectado" or clasificacion == "En revision":

        #extra va a valer lo que ponga el usuario
        extra = input("Escriba su numero de telefono(ejem: 0424 202 0449): ")

        #Si el tamaño de extra es mayor o igual que 4
        if len(extra) >= 4:

            #Si extra inicia con 0412 o 0414 o 0424 o 0416 o 0426
            if extra.find("0412") ==0 or  extra.find("0414") ==0 or extra.find("0424") ==0 or extra.find("0416") ==0 or extra.find("0426") ==0:

                #comprobador sera igual a False
                comprobador = False

                #Los espacios en blanco de extra se eliminaran y estara el numero unido
                extra = extra.replace(" ", "")

        #Mientras que extra no sea un numero natural o el tamaño de extra sea menor de 10 caracteres o comprobador sea True
        while not extra.isdecimal() or len(extra) <10 or comprobador:

            #extra va a valer lo que ponga el usuario
            extra = input("Escriba su numero de telefono(ejem: 0424 205 0449): ")

            #Si el tamaño de extra es mayor o igual que 4
            if len(extra) >= 4:

                #Si extra inicia con 0412 o 0414 o 0424 o 0416 o 0426
                if extra.find("0412") ==0 or  extra.find("0414") ==0 or extra.find("0424") ==0 or extra.find("0416") ==0 or extra.find("0426") ==0:

                    #comprobador sera igual a False
                    comprobador = False

                    #Los espacios en blanco de extra se eliminaran y estara el numero unido
                    extra = extra.replace(" ", "")

    #Sino, si clasificacion es Posible infectado o Infectado
    elif clasificacion == "Posible infectado" or clasificacion == "Infectado":

        #estado va a ser el valor que le de el usuario
        estado = input("Escriba el nombre de su estado: ")

        #ciudad va a ser el valor que le de el usuario
        ciudad = input("Escriba el nombre de su ciudad: ")

        #direccion va a ser el valor que le de el usuario
        direccion = input("Escriba el nombre de su direccion: ")

        #Si clasificacion es similar a Infectado
        if clasificacion == "Infectado":

            #Medico va ser el valor que le de el usuario
            medico = input("Escriba el nombre de su medico: ")

            #se crea la variable medico_espacios que va a ser una lista que contenga en elementos cada palabra de medico
            medico_espacios = medico.split()

            #Bucle de los elementos de medico_espacios
            for x in medico_espacios:

                #medico_nuevo va a ser todas las palabras de medico juntas
                medico_nuevo += x.lower().capitalize()

            #Mientras que medico_nuevo no sea alfabetico
            while not medico_nuevo.isalpha():

                #Medico va ser el valor que le de el usuario
                medico = input("Escriba el nombre de su medico: ")

                #se crea la variable medico_espacios que va a ser una lista que contenga en elementos cada palabra de medico
                medico_espacios = medico.split()

                #Bucle de los elementos de medico_espacios
                for x in medico_espacios:

                    #medico_nuevo va a ser todas las palabras de medico juntas
                    medico_nuevo += x.lower().capitalize()
        
        #Si clasificacion es similar a Posible infectado
        if clasificacion == "Posible infectado":

            #Extra va a ser igual a un string que dentro tiene los datos de estado, ciudad y direccion
            extra = (f"{estado}.{ciudad}.{direccion}")
        
        #Sino
        else: 

            #Extra va a ser igual a un string que dentro tiene los datos de estado, ciudad, direccion y medico_nuevo
            extra = (f"{estado}. {ciudad}. {direccion}. {medico_nuevo}")

    #Se crea el objeto Usuarios llamado usuario con la informacion de nombre_nuevo, edad, clasificacion,extra
    usuario = Usuarios(nombre_nuevo, edad, clasificacion,extra)

    #Se hace el llamado a un archivo de texto llamado almacenamiento.txt con la finalidad de agregar informacion y palabra clave alm
    with open("almacenamiento.txt", "a") as alm:

        #Se agrega la informacion del usuario en el archivo de texto
        alm.writelines(str(usuario))   
    
    
    return usuario

def moduloSAS2():
    """
    La funcion sirve para dar la informacion acerca del COVID-19 del pais que introduzca el usuario, ademas de hacer un top10 confirmados, muertos y recuperados con sus respectivos graficos

    confirmados: numero de personas confirmadas del COVID-19 del pais que eligio el usuario
    muertos: numero de personas muertas del COVID-19 del pais que eligio el usuario
    recuperados: numero de personas recuperadas del COVID-19 del pais que eligio el usuario
    todos_paises: es una lista que tiene todos los paises que tienen informacion acerca del COVID-19
    paises_diccionario_confirmados: es el diccionario con todos los paises como key y su cantidad de confirmados como value
    paises_diccionario_muertos: es el diccionario con todos los paises como key y su cantidad de muertos como value
    paises_diccionario_recuperados: es el diccionario con todos los paises como key y su cantidad de recuperados como value
    pais_nuevo: es el pais que se buscara para dar la informacion de confirmados, muertos y recuperados
    contador: Sirve para que los top10 y graficos no salgan mas de una vez
    """

    confirmados =0
    muertos =0
    recuperados =0
    todos_paises =[]
    paises_diccionario_confirmados ={} 
    paises_diccionario_muertos ={}
    paises_diccionario_recuperados ={} 
    pais_nuevo = ""
    contador = True

    #pais va a ser el valor que coloque el usuario
    pais = input("¿De cual pais quisieras tener informacion sobre el 'COVID-19'?\n")

    #Si el pais es distinto que US
    if pais != "US":

        #pais_espacios va a ser una lista con el valor de cada palabra de pais
        pais_espacios = pais.split()

        #Bucle de los elementos de pais_espacios
        for x in pais_espacios:
            
            #pais_nuevo va a tener el valor de cada palabra agregandole un espacio
            pais_nuevo += x + " "
        
        #pais nuevo no va a tener espacios en blanco al final o al inicio de la variable
        pais_nuevo = pais_nuevo.strip()
    
    #Sino, si el pais es similar a US
    elif pais == "US":

        #pais_nuevo va a ser igual que pais
        pais_nuevo = pais

    
    #Se hace el llamado de la API sobre el COVID-19
    url = "https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/stats"

    querystring = {"country":f"{pais_nuevo}"}#va a buscar el pais que seleccionamos

    headers = {
    'x-rapidapi-host': "covid-19-coronavirus-statistics.p.rapidapi.com",
    'x-rapidapi-key': "82d5c14fb2msh58e40d614a2173ep1fb57ejsnc01281a5506d"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    #Se transforma la respuesta en un diccionario
    diccionario = response.json()

    #Si el mensaje del diccionario es diferente a "OK"
    if diccionario["message"] != "OK":

        #Imprime que el pais_nuevo no esta en el registro
        print(f"{pais_nuevo} no se ha podido encontrar, recordar escribir el pais en ingles")
        
        #Bucle de los elementos del diccionario["data"]["covid19Stats"](estadisticas)
        for paises in diccionario["data"]["covid19Stats"]:

            #Pone todos los paises en una lista
            todos_paises.append(paises["country"])
    
        #Imprime la lista que se convierte en un set
        print("\nEstos son los paises validos\n",set(todos_paises))

        #Se hace el llamado a la funcion moduloSAS2()
        moduloSAS2()

        #contador va a valer 1
        contador = False

    #Sino
    else:

        #Bucle de los elementos diccionario["data"]["covid19Stats"](estadisticas)
        for provincia in diccionario["data"]["covid19Stats"]:

            confirmados += provincia["confirmed"]#confirmados va a se igual a la suma de el mismo y el valor "confirmed" del diccionario
            muertos += provincia["deaths"]#muertos va a se igual a la suma de el mismo y el valor "deaths" del diccionario
            recuperados += provincia["recovered"]#recuperados va a se igual a la suma de el mismo y el valor "recovered" del diccionario

        #imprime los confirmados, muertos y recuperados
        print(f"\ntotal:\nconfirmados: {confirmados}\nmuertos: {muertos}\nrecuperados: {recuperados}")

    #Si contador es True
    if contador:

        #Se hace el llamado de la API sobre el COVID-19
        url = "https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/stats"

        querystring = {"country":""}#Como no se selecciona pais, da toda la informacion

        headers = {
        'x-rapidapi-host': "covid-19-coronavirus-statistics.p.rapidapi.com",
        'x-rapidapi-key': "82d5c14fb2msh58e40d614a2173ep1fb57ejsnc01281a5506d"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        #Se transforma la respuesta en un diccionario
        diccionario = response.json()

        #Bucle  de los elementos de diccionario["data"]["covid19Stats"](estadisticas)
        for paises in diccionario["data"]["covid19Stats"]:

            #Pone todos los paises en una lista
            todos_paises.append(paises["country"])

        #Bucle del set de todos_paises(una lista de los paises sin repetirse)
        for paisesss in set(todos_paises):

            #todos los values de los diccionarios van a ser 0
            paises_diccionario_confirmados[paisesss] = 0 
            paises_diccionario_muertos[paisesss] = 0 
            paises_diccionario_recuperados[paisesss] = 0 

        #Bucle de los elementos (key y value) de paises_diccionario_confirmados
        for elemento_pais, cantidad in paises_diccionario_confirmados.items():

            #Bucle de los elementos de diccionario["data"]["covid19Stats"](estadisticas)
            for paises_total in diccionario["data"]["covid19Stats"]:

                #Si el elemento(key) del diccionario es similar a paises_total["country"](el pais del diccionario de la informacion)
                if paises_total["country"] == elemento_pais:

                    #el elemento de paises_diccionario_confirmados va a ser la suma de el mismo con los confirmados del diccionario con toda la informacion
                    paises_diccionario_confirmados[elemento_pais] += paises_total["confirmed"] 

        #Bucle de los elementos (key y value) de paises_diccionario_muertos
        for elemento_pais, cantidad in paises_diccionario_muertos.items():

            #Bucle de los elementos de diccionario["data"]["covid19Stats"](estadisticas)
            for paises_total in diccionario["data"]["covid19Stats"]:

                #Si el elemento(key) del diccionario es similar a paises_total["country"](el pais del diccionario de la informacion)
                if paises_total["country"] == elemento_pais:

                    #el elemento de paises_diccionario_muertos va a ser la suma de el mismo con los muertos del diccionario con toda la informacion
                    paises_diccionario_muertos[elemento_pais] += paises_total["deaths"] 

        #Bucle de los elementos (key y value) de paises_diccionario_recuperados
        for elemento_pais, cantidad in paises_diccionario_recuperados.items():

            #Bucle de los elementos de diccionario["data"]["covid19Stats"](estadisticas)
            for paises_total in diccionario["data"]["covid19Stats"]:

                #Si el elemento(key) del diccionario es similar a paises_total["country"](el pais del diccionario de la informacion)
                if paises_total["country"] == elemento_pais:

                    #el elemento de paises_diccionario_recuperados va a ser la suma de el mismo con los recuperados del diccionario con toda la informacion
                    paises_diccionario_recuperados[elemento_pais] += paises_total["recovered"] 

        print("\nTop 10 paises con mayor confirmados:")

        #se hace el llamado de la funcion top10 con el argumento paises_diccionario_confirmados
        top10(paises_diccionario_confirmados)

        print("\nTop 10 paises con mayor muertes:")

        #se hace el llamado de la funcion top10 con el argumento paises_diccionario_muertos
        top10(paises_diccionario_muertos)

        print("\nTop 10 paises con mayor recuperados:")

        #se hace el llamado de la funcion top10 con el argumento paises_diccionario_recuperados
        top10(paises_diccionario_recuperados)
    
def top10(diccionario):
    """
        La funcion sirve para que se haga el top 10 del diccionario que le pase(confirmados, muertos o recuperados) ademas de hacer una grafica con esa informacion

        \tArgumento=>"diccionario": es el diccionario al cual se utilizara para sacar el top10 y las graficas

        lista_pais: es una lista que va a contener los top 10 paises
        lista_cantidad: es el valor que tienen los top 10 paises
    """

    lista_pais =[]
    lista_cantidad=[]

    #Buble de 10 veces
    for x in range(10):

        top10 = ""
        top10_numero = 0
        maximo = 0

        #Bucle de los elementos en diccionario, que incluyen el key y el value
        for elemento,cantidad in diccionario.items():

            #Si cantidad es mayor que maximo
            if cantidad > maximo:

                top10 = elemento
                top10_numero = cantidad
                maximo = cantidad

        print(top10, top10_numero)

        lista_pais.append(top10) #se le agrega top10 a lista_pais(se le agrega el pais con el valor mas alto)
        lista_cantidad.append(top10_numero) #se le agrega top10_numero a lista_cantidad(el pais con la cantidad mas alta)

        #Se elimina el elemento(key) mas alto
        diccionario.pop(top10)

    plt.bar(lista_pais, lista_cantidad)#Hace una grafica de barras en donde el eje x son los paises y el eje y son las catidades
    plt.show()#muestra la grafica 

def verificar(name):
    """
        La funcion trata sobre verificar si el nombre de una persona esta en el archivo de texto, si esta el nombre, se borra todo el contenido para que se haga el nuevo

        \tArgumento=>"name": es el nombre que se compara en el archivo

        contador: sirve para saber en que lugar(linea) donde esta la informacion
    """

    contador =0

    #Se hace el llamado a un archivo de texto llamado almacenamiento.txt con la finalidad de leer informacion y palabra clave alm
    with open("almacenamiento.txt", "r") as alm:

        #bd va a ser una lista con la informacion que tiene el archivo de texto
        bd = alm.readlines()

    #Bucle de los elementos de bd
    for contenido in bd:

        #datos sera una lista y cada elemento sera separado por la palabra clave";"
        datos = contenido.split(";")

        #Si el primer elemento de datos(osea el nombre en el archivo de texto) es similar a name
        if datos[0] == name:

            #Se borra ese elemeto del archivo de texto
            bd.pop(contador)
        
        #Se suma 1 al contador
        contador += 1
    

    #Se hace el llamado a un archivo de texto llamado almacenamiento.txt con la finalidad de sobreescribir informacion y palabra clave alm
    with open("almacenamiento.txt", "w") as alm:

        #Se sobreescribe la informacion en el archivo de texto
        alm.writelines(bd)


def main():

    print(moduloSAS1())

    moduloSAS2()

main()