class Usuarios():

    def __init__(self,nombre,edad,status,extra):

        self.nombre = nombre
        self.edad = edad
        self.status = status
        self.extra = extra

    def __str__(self):

        return (f"{self.nombre}; {self.edad} anios; status: {self.status}; {self.extra}\n")